
import re

def isphonenumber(number):
    regex= "1?[\s-]?\(?(\d{3})\)?[\s-]?\d{3}[\s-]?\d{4}"
    if re.search(regex, number):
        return True
    return False

isphonenumber('415-555-1234')
isphonenumber('650-555-2345')
isphonenumber('(416)555-3456')
isphonenumber('202 555 4567')
isphonenumber('1 416 555 9292')
isphonenumber('4035555678')
isphonenumber('123456778bc')
isphonenumber('123abc456')
isphonenumber('456abc789')
isphonenumber('abc1234')
isphonenumber('/adxgfhgf1235')
isphonenumber('123 456 7890')
isphonenumber('145 678-8989')
isphonenumber('(456)555 4560')

def isnumber(number):
    regex = '^[+-]?e?(\d+,?)(.\d+)*(e\d+)?(e-\d+)?$'
    if re.search(regex, number):
        return True
    return False

isnumber('3.14529')
isnumber('720p')
isnumber('1.9e10')
isnumber('1.abc12')
isnumber('128')
isnumber('123,340.00')
isnumber('13e+456e+789')
isnumber('a123b4')
isnumber('1e12')
isnumber('d123')
isnumber('1.45e')
isnumber('1e12')
isnumber('e1.45')
isnumber('10,40,500')
isnumber('+456412')
isnumber('-158678')
isnumber('-1612')
isnumber('+1e12')
isnumber('1e-100')
isnumber('e100')
