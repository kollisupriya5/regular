import unittest
import regular_functions as regfun

class testmethod(unittest.TestCase):
    def test1(self):
        self.assertEqual(regfun.isphonenumber('415-555-1234'),True)
    def test2(self):
        self.assertEqual(regfun.isphonenumber('650-555-2345'),True)
    def test3(self):
        self.assertEqual(regfun.isphonenumber('(416)555-3456'),True)
    def test4(self):
        self.assertEqual(regfun.isphonenumber('202 555 4567'),True)
    def test5(self):
        self.assertEqual(regfun.isphonenumber('1 416 555 9292'),True)
    def test6(self):
        self.assertEqual(regfun.isphonenumber('4035555678'),True)
    def test7(self):
        self.assertEqual(regfun.isphonenumber('4567891012'),True)
    def test8(self):
        self.assertEqual(regfun.isphonenumber('123456778bc'),False)
    def test9(self):
        self.assertEqual(regfun.isphonenumber('123abc456'),False)
    def test10(self):
        self.assertEqual(regfun.isphonenumber('456abc789'),False)
    def test11(self):
        self.assertEqual(regfun.isphonenumber('abc1234'),False)
    def test12(self):
        self.assertEqual(regfun.isphonenumber('/adxgfhgf1235'),False)
    def test13(self):
        self.assertEqual(regfun.isphonenumber('123 456 7890'),True)
    def test14(self):
        self.assertEqual(regfun.isphonenumber('145 678-8989'),True)
    def test15(self):
        self.assertEqual(regfun.isphonenumber('(456)555 4560'),True)

    def test16(self):
        self.assertEqual(regfun.isnumber('19,69,456'), True)
    def test17(self):
        self.assertEqual(regfun.isnumber('+456412'), True)
    def test18(self):
        self.assertEqual(regfun.isnumber('e123'),True)
    def test19(self):
        self.assertEqual(regfun.isnumber('1e-100'),True)
    def test20(self):
        self.assertEqual(regfun.isnumber('1'),True)
    def test21(self):
        self.assertEqual(regfun.isnumber('e123'),True)
    def test22(self):
        self.assertEqual(regfun.isnumber('3.1425'),True)
    def test23(self):
        self.assertEqual(regfun.isnumber('123,340.00'),True)
    def test24(self):
        self.assertEqual(regfun.isnumber('1e12'),True)
    def test25(self):
        self.assertEqual(regfun.isnumber('-1e12'),True)
    def test26(self):
        self.assertEqual(regfun.isnumber('1.45e'),False)
    def test27(self):
        self.assertEqual(regfun.isnumber('d123'),False)
    def test28(self):
        self.assertEqual(regfun.isnumber('abc123'),False)
    def test29(self):
        self.assertEqual(regfun.isnumber('412bc4'),False)
    def test30(self):
        self.assertEqual(regfun.isnumber('130e+15e+123'),False)




if __name__ == '__main__':
    unittest.main()
